--Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?
SELECT 
    store_id,
    staff_id,
    total_revenue
FROM (
    SELECT 
        s.store_id,
        p.staff_id,
        SUM(p.amount) AS total_revenue,
        RANK() OVER (PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) AS revenue_rank
    FROM 
        payment p
    JOIN rental r ON p.rental_id = r.rental_id
    JOIN staff s ON p.staff_id = s.staff_id
    WHERE 
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY 
        s.store_id, p.staff_id
) ranked_revenue
WHERE 
    revenue_rank = 1
ORDER BY
    store_id, staff_id;


 
--Which five movies were rented more than the others, and what is the expected age of the audience for these movies?
SELECT
    film_id,
    title,
    rating,
    rental_count,
    CASE
        WHEN rating = 'G' THEN 'All ages'
        WHEN rating = 'PG' THEN '10 or more'
        WHEN rating = 'PG-13' THEN '13 or more'
        WHEN rating = 'R' THEN '17 or more'
        WHEN rating = 'NC-17' THEN '18 or more'
        ELSE 'Unknown'
    END AS expected_age
FROM (
    SELECT
        f.film_id,
        f.title,
        f.rating,
        COUNT(r.rental_id) AS rental_count,
        RANK() OVER (ORDER BY COUNT(r.rental_id) DESC) AS rank
    FROM
        film f
        JOIN inventory i ON f.film_id = i.film_id
        JOIN rental r ON i.inventory_id = r.inventory_id
    GROUP BY
        f.film_id, f.title, f.rating
) AS ranked_movies
WHERE
    rank <= 5
ORDER BY
    rental_count DESC
LIMIT 5;




--Which actors/actresses didn't act for a longer period of time than the others?
SELECT
    actor_id,
    first_name,
    last_name,
    last_film_year
FROM (
    SELECT
        a.actor_id,
        a.first_name,
        a.last_name,
        MAX(f.release_year) AS last_film_year,
        RANK() OVER (ORDER BY MAX(f.release_year) ASC) AS rank
    FROM
        actor a
        JOIN film_actor fa ON a.actor_id = fa.actor_id
        JOIN film f ON fa.film_id = f.film_id
    GROUP BY
        a.actor_id, a.first_name, a.last_name
) AS ranked_actors
WHERE
    rank <= 5
ORDER BY
    last_film_year asc;